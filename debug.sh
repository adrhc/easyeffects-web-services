#!/bin/bash

# ./debug.sh | tee easyeffects-web-services.log

# -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"
# ./mvnw spring-boot:run -Dspring.config.additional-location=$HOME/Projects-adrhc/easyeffects-web-services/config/ --debug

# $HOME/.sdkman/candidates/java/current/bin/java --enable-preview -jar /fast-disk/java-apps/easyeffects-web-services-1.0-SNAPSHOT.jar --spring.config.additional-location=$HOME/Projects-adrhc/easyeffects-web-services/config/

# config/application.yml is automatically detected when running from $HOME/Projects-adrhc/easyeffects-web-services
cd $HOME/Projects-adrhc/easyeffects-web-services

./mvnw spring-boot:run -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"
