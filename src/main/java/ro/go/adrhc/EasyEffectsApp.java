package ro.go.adrhc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class EasyEffectsApp {
    public static void main(String[] args) {
        SpringApplication.run(EasyEffectsApp.class, args);
    }
}
