package ro.go.adrhc.infrastructure;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.text.ParseException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@ConfigurationProperties("easyeffects")
@Getter
@Setter
@ToString
@Slf4j
public class EEProperties {
    private Duration cmdTimeout;
    private Duration changeTimeout;
    private String cmd;
    private String loadPreset;
    private String presets;
    private String gsettingsCmd;
    private List<String> lastUsed;
    private MessageFormat lastUsedPattern;
    private Path outputPresetsPath;

    public List<String> getLoadPreset(String preset) {
        return List.of(cmd, loadPreset, preset);
    }

    public List<String> getPresets() {
        return List.of(cmd, presets);
    }

    public List<String> getLastUsed() {
        List<String> params = new ArrayList<>(lastUsed);
        params.add(0, gsettingsCmd);
        return params;
    }

    public void setLastUsedPattern(String pattern) {
        this.lastUsedPattern = new MessageFormat(pattern);
    }

    public Optional<String> parseLastPreset(String text) {
        try {
            return Optional.of((String) lastUsedPattern.parse(text)[0]);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
            return Optional.empty();
        }
    }

    public Path getOutputPresetPath(String preset) {
        return outputPresetsPath.resolve(preset + ".json");
    }

    public Optional<String> getOutputPreset(String preset) {
        try {
            return Optional.of(Files.readString(getOutputPresetPath(preset)));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return Optional.empty();
        }
    }
}
