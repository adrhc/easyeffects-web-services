package ro.go.adrhc.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import ro.go.adrhc.infrastructure.domain.EEPresets;
import ro.go.adrhc.infrastructure.domain.EEPresetsParser;
import ro.go.adrhc.infrastructure.domain.Preset;
import ro.go.adrhc.infrastructure.domain.PresetParser;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static java.util.concurrent.TimeUnit.SECONDS;

@Service
@RequiredArgsConstructor
@Slf4j
public class EEService {
    private final EEProperties properties;
    private final ObjectMapper mapper;
    private final EEPresetsParser presetsParser;
    private final PresetParser presetParser;

    public Optional<String> getLastUsed() {
        return execute(properties::getLastUsed).flatMap(properties::parseLastPreset);
    }

    public void loadPreset(String preset) {
        runCommand(() -> properties.getLoadPreset(preset));
    }

    public Optional<EEPresets> getPresets() {
        Optional<String> outcome = execute(properties::getPresets);
        return outcome.map(presetsParser::parse);
    }

    private Optional<String> execute(Supplier<List<String>> paramsSupplier) {
        ProcessBuilder processBuilder = createProcessBuilder(paramsSupplier);
        try {
            Process process = processBuilder.start();
            String output = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8);
            log.info("\n{}:\n{}", processBuilder.command(), output);
            // process waiting must happen after reading its output!
            process.waitFor(properties.getCmdTimeout().getSeconds(), SECONDS);
            return Optional.of(output);
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage(), e);
            return Optional.empty();
        }
    }

    private void runCommand(Supplier<List<String>> paramsSupplier) {
        ProcessBuilder processBuilder = createProcessBuilder(paramsSupplier);
        try {
            Process process = processBuilder.start();
            // process waiting must happen after reading its output!
            process.waitFor(properties.getCmdTimeout().getSeconds(), SECONDS);
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }

    private ProcessBuilder createProcessBuilder(Supplier<List<String>> paramsSupplier) {
        ProcessBuilder processBuilder = new ProcessBuilder(paramsSupplier.get());
        processBuilder.redirectInput(ProcessBuilder.Redirect.INHERIT);
        processBuilder.redirectError(ProcessBuilder.Redirect.INHERIT);
        return processBuilder;
    }

    public Optional<Preset> getPreset(String presetName) {
        return properties.getOutputPreset(presetName).flatMap(presetParser::parse);
    }

    public void updatePreset(String presetName, Preset preset) throws IOException {
        Preset mergedPreset = getPreset(presetName).map(preset::mergeInto)
                .orElseThrow(() -> new IOException("Can't change \"%s\"!".formatted(presetName)));
        log.info("\nNew value for \"{}\":\n{}", presetName, mergedPreset);
        Files.writeString(properties.getOutputPresetPath(presetName),
                mapper.writeValueAsString(mergedPreset));
    }

    public void removePreset(String preset) throws IOException {
        Files.deleteIfExists(properties.getOutputPresetPath(preset));
    }
}
