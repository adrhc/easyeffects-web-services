package ro.go.adrhc.infrastructure.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.math.BigDecimal;

@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
public record BassEnhancer(BigDecimal amount, BigDecimal blend, boolean bypass,
        BigDecimal floor, boolean floorActive, BigDecimal harmonics,
        BigDecimal inputGain, BigDecimal outputGain, BigDecimal scope) {
    public static BassEnhancer of(BigDecimal amount, BigDecimal blend,
            BigDecimal harmonics, BigDecimal floor, BigDecimal scope) {
        return new BassEnhancer(amount, blend, false, floor,
                floor.compareTo(BigDecimal.ZERO) > 0, harmonics,
                BigDecimal.ZERO, BigDecimal.ZERO, scope);
    }
}
