package ro.go.adrhc.infrastructure.domain;

import java.util.Set;

public record EEPresets(Set<String> input, Set<String> output) {
}
