package ro.go.adrhc.infrastructure.domain;

import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class EEPresetsParser {
    public EEPresets parse(String text) {
        String[] lines = text.split("\n");
        return new EEPresets(parsePresets(lines[1]), parsePresets(lines[0]));
    }

    private Set<String> parsePresets(String line) {
        return new Scanner(line.substring(line.indexOf(':') + 1).trim())
                .useDelimiter(",")
                .tokens()
                .collect(Collectors.toSet());
    }
}
