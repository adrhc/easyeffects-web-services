package ro.go.adrhc.infrastructure.domain;

public record Preset(PresetOutput output) {
    public Preset mergeInto(Preset into) {
        PresetOutput toOutput = into.output();
        BassEnhancer toBE = toOutput.bassEnhancer();
        BassEnhancer fromBE = output.bassEnhancer();
        return new Preset(new PresetOutput(new BassEnhancer(fromBE.amount(),
                fromBE.blend(), toBE.bypass(), fromBE.floor(), fromBE.floorActive(),
                fromBE.harmonics(), toBE.inputGain(), toBE.outputGain(),
                fromBE.scope()), toOutput.blocklist(), toOutput.pluginsOrder()));
    }
}
