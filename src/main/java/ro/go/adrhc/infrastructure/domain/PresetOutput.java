package ro.go.adrhc.infrastructure.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public record PresetOutput(
        @JsonProperty("bass_enhancer#0") BassEnhancer bassEnhancer,
        List<Object> blocklist, List<String> pluginsOrder) {
    public static PresetOutput of(BassEnhancer bassEnhancer) {
        return new PresetOutput(bassEnhancer, List.of(), List.of("bass_enhancer#0"));
    }
}
