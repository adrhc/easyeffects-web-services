package ro.go.adrhc.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.core.ConditionTimeoutException;
import org.springframework.stereotype.Service;
import ro.go.adrhc.infrastructure.EEProperties;
import ro.go.adrhc.infrastructure.EEService;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;
import java.util.function.Predicate;

import static org.awaitility.Awaitility.await;

@Service
@RequiredArgsConstructor
@Slf4j
public class EEManager {
    private final EEProperties properties;
    private final EEService eeService;

    public Optional<PresetDto> getPreset(String preset) {
        return eeService.getPreset(preset).map(p -> PresetDto.of(preset, p));
    }

    public Optional<PresetDto> updatePreset(PresetDto presetDto) {
        try {
            eeService.updatePreset(presetDto.name(), presetDto.toPreset());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return getPreset(presetDto.name());
    }

    public Optional<String> loadPreset(String preset) {
        eeService.loadPreset(preset);
        try {
            return awaitStateMatch(preset::equals);
        } catch (ConditionTimeoutException e) {
            return eeService.getLastUsed();
        }
    }

    private Optional<String> awaitStateMatch(Predicate<String> predicate) {
        return await()
                .atLeast(Duration.ofSeconds(1))
                .atMost(properties.getChangeTimeout())
                .with()
                .pollInterval(Duration.ofSeconds(1))
                .until(eeService::getLastUsed, it -> it.stream().anyMatch(predicate));
    }
}
