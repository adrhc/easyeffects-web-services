package ro.go.adrhc.managers;

import ro.go.adrhc.infrastructure.domain.BassEnhancer;
import ro.go.adrhc.infrastructure.domain.Preset;
import ro.go.adrhc.infrastructure.domain.PresetOutput;

import java.math.BigDecimal;

public record PresetDto(String name, BigDecimal amount, BigDecimal blend,
        BigDecimal harmonics, BigDecimal floor, BigDecimal scope) {
    public static PresetDto of(String presetName, Preset preset) {
        BassEnhancer be = preset.output().bassEnhancer();
        return new PresetDto(presetName, be.amount(), be.blend(), be.harmonics(), be.floor(), be.scope());
    }

    public Preset toPreset() {
        BassEnhancer bassEnhancer = BassEnhancer.of(amount, blend, harmonics, floor, scope);
        PresetOutput output = PresetOutput.of(bassEnhancer);
        return new Preset(output);
    }
}
