package ro.go.adrhc.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ro.go.adrhc.infrastructure.EEService;
import ro.go.adrhc.infrastructure.domain.EEPresets;
import ro.go.adrhc.managers.EEManager;
import ro.go.adrhc.managers.PresetDto;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/api/ee")
@RequiredArgsConstructor
@Slf4j
public class EasyEffectsRestController {
    private final EEService eeService;
    private final EEManager eeManager;

    @DeleteMapping("{preset}")
    public void removePreset(@PathVariable("preset") String preset) throws IOException {
        eeService.removePreset(preset);
    }

    @GetMapping("{preset}")
    public Optional<PresetDto> getPreset(@PathVariable("preset") String preset) {
        return eeManager.getPreset(preset);
    }

    @PostMapping("{preset}")
    public Optional<PresetDto> updatePreset(@RequestBody PresetDto preset) {
        return eeManager.updatePreset(preset);
    }

    @PostMapping("{preset}/load")
    public Optional<String> loadPreset(@PathVariable("preset") String preset) {
        return eeManager.loadPreset(preset);
    }

    @RequestMapping("last-used")
    public Optional<String> getLastUsed() {
        return eeService.getLastUsed();
    }


    @RequestMapping
    public Optional<EEPresets> getPresets() {
        return eeService.getPresets();
    }
}
