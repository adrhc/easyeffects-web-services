package ro.go.adrhc;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Unit test for simple EasyEffectsApp.
 */
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EasyEffectsAppTest {
    @Test
    void contextLoads() {
    }
}
