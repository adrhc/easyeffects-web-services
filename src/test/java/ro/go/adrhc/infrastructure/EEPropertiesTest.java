package ro.go.adrhc.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class EEPropertiesTest {
    @Autowired
    private EEProperties properties;

    @Test
    void parseLastPreset() {
        Optional<String> lastPreset = properties.parseLastPreset("'music'");
        assertThat(lastPreset).hasValue("music");
        assertThat(properties.getOutputPresetsPath()).isNotNull();
    }
}
