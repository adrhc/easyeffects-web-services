package ro.go.adrhc.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ro.go.adrhc.infrastructure.domain.BassEnhancer;
import ro.go.adrhc.infrastructure.domain.EEPresets;
import ro.go.adrhc.infrastructure.domain.Preset;

import java.util.Optional;
import java.util.Set;

import static java.util.function.Predicate.not;
import static org.assertj.core.api.Assertions.assertThat;

@DisabledOnOs(OS.WINDOWS)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class EEServiceIT {
    @Autowired
    private EEService eeService;

    @Test
    void getPreset() {
        Optional<Preset> preset = eeService.getPreset("music");
        assertThat(preset).isNotEmpty();
        log.info("\n\"music\" preset: {}", preset.get());
        BassEnhancer bassEnhancer = preset.get().output().bassEnhancer();
        assertThat(bassEnhancer.amount()).isNotNull();
    }

    @Test
    void getLastUsed() {
        Optional<String> profile = eeService.getLastUsed();
        assertThat(profile).isNotEmpty();
        log.info("\nlast used preset: {}", profile.get());
        assertThat(profile).hasValueSatisfying(
                new Condition<>(not(String::isBlank), "not empty text"));
    }

    @Test
    void getPresets() {
        Optional<EEPresets> presets = eeService.getPresets();
        assertThat(presets).isNotEmpty();
        log.info("\npresets: {}", presets.get());
        assertThat(presets).map(EEPresets::output).hasValue(Set.of());
    }
}