package ro.go.adrhc.infrastructure.domain;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class EEPresetsParserTest {
    @Autowired
    private EEPresetsParser parser;

    @Test
    void parse() {
        EEPresets presets = parser.parse("""
                Output Presets: movies1,movies2,music,
                Input Presets:""");
        assertThat(presets.output()).containsOnly("movies1", "movies2", "music");
        assertThat(presets.input()).isEmpty();
    }
}