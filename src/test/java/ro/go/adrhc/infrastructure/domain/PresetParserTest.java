package ro.go.adrhc.infrastructure.domain;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class PresetParserTest {
    public static String MUSIC = """
            {
                  "output": {
                      "bass_enhancer#0": {
                          "amount": 2.7755575615628914e-17,
                          "blend": 0.10,
                          "bypass": false,
                          "floor": 60.0,
                          "floor-active": true,
                          "harmonics": 7.100000000000001,
                          "input-gain": 0.20,
                          "output-gain": 0.30,
                          "scope": 165.0
                      },
                      "blocklist": [],
                      "plugins_order": [
                          "bass_enhancer#0"
                      ]
                  }
            }""";
    @Autowired
    private PresetParser parser;

    @Test
    void parse() {
        Optional<Preset> preset = parser.parse(MUSIC);
        assertThat(preset.map(Preset::output).map(PresetOutput::bassEnhancer))
                .hasValueSatisfying(new Condition<>(
                        it -> eqBD("2.7755575615628914e-17", it.amount())
                                && eqBD("0.10", it.blend())
                                && !it.bypass()
                                && eqBD("60.0", it.floor())
                                && it.floorActive()
                                && eqBD("7.100000000000001", it.harmonics())
                                && eqBD("0.20", it.inputGain())
                                && eqBD("0.30", it.outputGain())
                                && eqBD("165.0", it.scope()),
                        "bassEnhancer value should match!"));
    }

    private boolean eqBD(String expected, BigDecimal actual) {
        return actual.equals(new BigDecimal(expected));
    }
}