package ro.go.adrhc.infrastructure.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ro.go.adrhc.managers.PresetDto;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.infrastructure.domain.PresetParserTest.MUSIC;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class PresetTest {
    public static String EXPECTATION = "{\"output\":{\"bass_enhancer#0\":{\"amount\":17.77,\"blend\":-5.0,\"bypass\":false,\"floor\":37.77,\"floor-active\":true,\"harmonics\":27.77,\"input-gain\":0.20,\"output-gain\":0.30,\"scope\":47.77},\"blocklist\":[],\"plugins_order\":[\"bass_enhancer#0\"]}}";
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private PresetParser parser;

    @Test
    void mergeInto() throws JsonProcessingException {
        PresetDto presetDto = new PresetDto(
                "musicX", new BigDecimal("17.77"),
                new BigDecimal("-5.0"), new BigDecimal("27.77"),
                new BigDecimal("37.77"), new BigDecimal("47.77"));
        Optional<Preset> preset = parser.parse(MUSIC).map(presetDto.toPreset()::mergeInto);
        assertThat(preset).isNotEmpty();
        String actual = mapper.writeValueAsString(preset.get());
        assertThat(actual).isEqualTo(EXPECTATION);
    }
}